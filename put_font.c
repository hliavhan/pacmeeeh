/*******************************************************************
  put_font.c - simple program which print fonts to display
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/
#include "put_font.h"
#include "font_types.h"
#include "sprites.h"
#include "pixels.h"
#include <string.h> 


unsigned char_width(font_descriptor_t* fdes, int ch) {
  int width = 0;
  if ((ch >= fdes->firstchar) && (ch-fdes->firstchar < fdes->size)) {
    ch -= fdes->firstchar;
    if (!fdes->width) {
      width = fdes->maxwidth;
    } else {
      width = fdes->width[ch];
    }
  }
  return width;
}
void print_on_LCD(char text[], union pixel menu_buffer[LCD_WIDTH][LCD_HEIGHT], union pixel color, int height, int width){
	for(unsigned i = 0; i< strlen(text); i++){
		pchar(&font_rom8x16, text[i], width + i*(&font_rom8x16)->maxwidth, height, menu_buffer, color);
	}
}

void pchar(font_descriptor_t *font, char c, unsigned x, unsigned y, union pixel buffer[480][320], union pixel color){
	for (unsigned w = 0; w < font->maxwidth; w++)
	{
		for(unsigned h = 0; h < font->height; h++)
		{
			if(font->bits[(c - font->firstchar) * font->height + h] & (1 << (16 - w)))
			{
				buffer[x + w][y + h] = color;
			}
		}
	}
}
