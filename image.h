/*******************************************************************
  image.h - simple function to redraw display
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "sprites.h"
/// Redraw full display
///@param buffer buffer with which one will be fill display
void redraw_display(union pixel buffer[LCD_WIDTH][LCD_HEIGHT]);

extern unsigned char *parlcd_reg_base;
