/*******************************************************************
  sprites.h - read file and load images for backgrounds and sprites
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "pixels.h"


#define BG_COL_R 0
#define BG_COL_G 0
#define BG_COL_B 0
#define BG_COL 0x000000

#define WALL_COL_R 33
#define WALL_COL_G 33
#define WALL_COL_B 255
#define WALL_COL 0x2121ff

#define FOOD_COL_R 255
#define FOOD_COL_G 183
#define FOOD_COL_B 174
#define FOOD_COL 0xffb7ae

#define BIG_FOOD_COL_R 0
#define BIG_FOOD_COL_G 255
#define BIG_FOOD_COL_B 0
#define BID_FOOD_COL 0x00ff00

#define DOOR_COL_R 255
#define DOOR_COL_G 255
#define DOOR_COL_B 255
#define DOOR_COL 0xffffff

#define PACMEH_COL_R 255
#define PACMEH_COL_G 255
#define PACMEH_COL_B 0
#define PACMEH_COL 65504

#ifndef GHOSTS
#define GHOSTS

#define R_GH_R 255
#define R_GH_G 0
#define R_GH_B 0
#define R_GH_COL 0xff0000

#define B_GH_R 0
#define B_GH_G 255
#define B_GH_B 255
#define B_GH_COL 0x00ffff

#define P_GH_R 255
#define P_GH_G 183
#define P_GH_B 255
#define P_GH_COL 0xffb7ff

#define O_GH_R 255
#define O_GH_G 183
#define O_GH_B 81
#define O_GH_COL 0xffb751

#endif

#define LCD_WIDTH 480
#define LCD_HEIGHT 320

#define FILE_ERROR 100
#define READ_ERROR 101

extern union pixel buffer[LCD_WIDTH][LCD_HEIGHT];
extern union pixel newbuf[LCD_WIDTH][LCD_HEIGHT];

#ifndef SPRITES_H
#define SPRITES_H

struct sprite_pixel{
	unsigned int coord[2];
	uint16_t col;
};
/// Function load background image
///@param fname filename of file with sprite image 
///@param buffer buffer with data image
void load_level_map(char* fname, union pixel buffer[LCD_WIDTH][LCD_HEIGHT]);
/// Function load food image
///@param fname filename of file with sprite image 
///@param buffer buffer with data image
int load_food_map(char* fname, union pixel buffer[LCD_WIDTH][LCD_HEIGHT]);
/// Function load sprite image with start coordinats. Coordinates read from image with full level
///@param fname filename of file with sprite image 
///@param loaded_map loaded map data with all elements positions
///@param sprite buffer for sprite data(pixels colors + coorinates)
void sprite(char* fname, union pixel loaded_map[LCD_WIDTH][LCD_HEIGHT], struct sprite_pixel sprite[]);

#endif 
