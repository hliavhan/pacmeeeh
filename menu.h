/*******************************************************************
  menu.h - draw menu and control level selection
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "pixels.h"
#include "sprites.h"
#include "put_font.h"
#include "font_types.h"


#define WIDTH 80
#define HEIGHT 30

#define LVL1 "LEVEL 1"
#define LVL2 "LEVEL 2"
#define LVL3 "LEVEL 3"
#define EXIT "TO EXIT GAME PRESS THE RED BUTTON"
#define INSTRUCTION "FOR STARTING GAME CHOSE LEVEL WITH GREEN KNOB"
#define INSTRUCTION2 "TO CONTROL PACMEEEH USE ONLY RED AND BLUE KNOBS!"
#define INSTRUCTION3 "GREEN ONE WILL NOT BE AVALIBLE DURING THE GAME"
#define COUNTER "POINTS: "

#ifndef MENU_H
#define MENU_H

/// Function draw simple reactangle button with text inside
///@param x x position of left top corner on display
///@param y y position of left top corner on display
///@param buffer buffer of image in which will be added buttons
///@param is_select boolean flag of selecion
///@param lvl_num number of level
void print_button(int x, int y, union pixel buffer[LCD_WIDTH][LCD_HEIGHT], bool is_select, int lvl_num);
/// Selection controls with central(green) rotary selector. Pressing on it selects the level
///@param new_value updated value of knob
///@param old_value previous value of knob
///@param select_button int flag of level number
///@param menu_buffer image buffer witn data of menu window
int level_select(uint8_t new_value, uint8_t old_value, int select_button, union pixel menu_buffer[LCD_WIDTH][LCD_HEIGHT]);
/// Print text on button
///@param x x position of left top corner pixel on display
///@param y y position of left top corner pixel on display 
///@param buffer buffer of image in which will be added text
///@param lvl_num number of level
void print_button_text(int x, int y, union pixel buffer[LCD_WIDTH][LCD_HEIGHT], int lvl_num);
/// Print score counter(count a number of eaten food)
///@param counter number of eaten food
///@param menu_buffer image buffer witn data of menu window
void counter_print(int counter, union pixel menu_buffer[LCD_WIDTH][LCD_HEIGHT]);

#endif
