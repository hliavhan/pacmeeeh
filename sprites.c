/*******************************************************************
  sprites.h - read file and load images for backgrounds and sprites
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "pixels.h"
#include "sprites.h"


void load_level_map(char* fname, union pixel buffer[LCD_WIDTH][LCD_HEIGHT]){
	// Open file for reading
	FILE *img = fopen(fname, "rb");
	if(img == NULL){
        fprintf(stderr, "ERROR: Cannot open file!\n");
        exit(FILE_ERROR);
    }
	int width, height, col;
	// Miss P6 line
    fseek(img, 3, SEEK_SET);
    // Reading width, height and col lines from file
    if(fscanf(img, "%d %d %d", &width, &height, &col) < 3){
        printf("ERROR of reading file! Probably file is not ppm.\n");
        exit(READ_ERROR);
    }
	// Miss '\n' symbol after parametrs
    fgetc(img);
	
	// Get ppm file to array of unsigned char. Every pixel is represent with 3 elements: R, G, B
	unsigned char *img_data = (unsigned char*)malloc(width*height*3*sizeof(unsigned char));
    if(fread(img_data, sizeof(char), width*height*3, img) != width*height*3)
		exit(READ_ERROR);
    fclose(img);
    
	// Remake RGB parametrs to 5-6-5 and write them to pixel array which will be print on display
	for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
			unsigned char red = img_data[(y * width + x) * 3];
			unsigned char green = img_data[(y * width + x) * 3 + 1];
			unsigned char blue = img_data[(y * width + x) * 3 + 2];
			buffer[x][y].d = (blue >> 3)| ((green & 0b11111100) << 3) | ((red & 0b11111000) << 8) ;
		}
	}
	free(img_data);
}


int load_food_map(char* fname, union pixel buffer[LCD_WIDTH][LCD_HEIGHT]){
	// Open file for reading
	FILE *img = fopen(fname, "rb");
	if(img == NULL){
        fprintf(stderr, "ERROR: Cannot open file!\n");
        exit(FILE_ERROR);
    }
	int width, height, col;
	// Miss P6 line
    fseek(img, 3, SEEK_SET);
    // Reading width, height and col lines from file
    if(fscanf(img, "%d %d %d", &width, &height, &col) < 3){
        printf("ERROR of reading file! Probably file is not ppm.\n");
        exit(READ_ERROR);
    }
	// Miss '\n' symbol after parametrs
    fgetc(img);
	int counter = 0;
	// Get ppm file to array of unsigned char. Every pixel is represent with 3 elements: R, G, B
	unsigned char *img_data = (unsigned char*)malloc(width*height*3*sizeof(unsigned char));
    if(fread(img_data, sizeof(char), width*height*3, img) != width*height*3)
		exit(READ_ERROR);
    fclose(img);
    
	// Remake RGB parametrs to 5-6-5 and write them to pixel array which will be print on display
	for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
			unsigned char red = img_data[(y * width + x) * 3];
			unsigned char green = img_data[(y * width + x) * 3 + 1];
			unsigned char blue = img_data[(y * width + x) * 3 + 2];
			if(!((red == 0) && (green == 0) && (blue == 0))){
				counter++;
			}
			buffer[x][y].d = (blue >> 3)| ((green & 0b11111100) << 3) | ((red & 0b11111000) << 8) ;
		}
	}
	free(img_data);
	return counter;
}


void sprite(char* fname, union pixel loaded_map[LCD_WIDTH][LCD_HEIGHT], struct sprite_pixel sprite[]){
	/*// Open file with sprite
	FILE *img_of_sprite = fopen(fname, "rb");
	if(img_of_sprite == NULL){
        fprintf(stderr, "ERROR: Cannot open file!\n");
        exit(FILE_ERROR);
     }
    int width, height, col;
	// Miss P6 line
    fseek(img_of_sprite, 3, SEEK_SET);
    // Reading width, height and col lines from file
    if(fscanf(img_of_sprite, "%d %d %d", &width, &height, &col) < 3){
        printf("ERROR of reading file! Probably file is not ppm.\n");
        exit(READ_ERROR);
    }
	// Miss '\n' symbol after parametrs
    fgetc(img_of_sprite);

    // Get ppm file to array of unsigned char. Every pixel is represent with 3 elements: R, G, B
    unsigned char *img_data = (unsigned char*)malloc(width*height*3*sizeof(unsigned char));
    if(fread(img_data, sizeof(char), width*height*3, img_of_sprite) != width*height*3)
		exit(READ_ERROR);
    fclose(img_of_sprite);*/
	unsigned char *img_data;
	int width, height;
	read_file(fname, *img_data, &width, &height);
    // Array of sprite_pixel contain only colorful pixels of assets + their (x, y) position. 
	// Size of array is defined by dimensions of element
    int i = 0;
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
			int red = img_data[(y * width + x) * 3];
			int green = img_data[(y * width + x) * 3 + 1];
			int blue =img_data[(y * width + x) * 3 + 2];
			if(!((red == 0) && (green == 0) && (blue == 0))){
				unsigned char red = img_data[(y * width + x) * 3];
				unsigned char green = img_data[(y * width + x) * 3 + 1];
				unsigned char blue = img_data[(y * width + x) * 3 + 2];
				sprite[i].col = (blue >> 3)| ((green & 0b11111100) << 3) | ((red & 0b11111000) << 8);
				i++;
			}else sprite[i].col = 0x0;
		}
	}
	free(img_data);
	// Coordinates is define in full image of level
	i = 0;
	for (int y = 0; y < LCD_HEIGHT; y++)
    {
        for (int x = 0; x < LCD_WIDTH; x++)
        {
			if(loaded_map[x][y].d == sprite[i].col && sprite[i].col != 0x0){
				sprite[i].coord[0] = x;
				sprite[i].coord[1] = y;
				i++;
			}
		}
	}
}

void read_file(char* fname, unsigned char *img_data, int* width, int* height){
	// Open file with sprite
	FILE *img_of_sprite = fopen(fname, "rb");
	if(img_of_sprite == NULL){
        fprintf(stderr, "ERROR: Cannot open file!\n");
        exit(FILE_ERROR);
     }
    int col;
	// Miss P6 line
    fseek(img_of_sprite, 3, SEEK_SET);
    // Reading width, height and col lines from file
    if(fscanf(img_of_sprite, "%d %d %d", width, height, &col) < 3){
        printf("ERROR of reading file! Probably file is not ppm.\n");
        exit(READ_ERROR);
    }
	// Miss '\n' symbol after parametrs
    fgetc(img_of_sprite);

    // Get ppm file to array of unsigned char. Every pixel is represent with 3 elements: R, G, B
    img_data = (unsigned char*)malloc((*width)*(*height)*3*sizeof(unsigned char));
    
    if(fread(img_data, sizeof(char), (*width)*(*height)*3, img_of_sprite) != (*width)*(*height)*3)
		exit(READ_ERROR);
    fclose(img_of_sprite);
}
