/*******************************************************************
  menu.c - draw menu and control level selection
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "menu.h"
#include "pixels.h"
#include "sprites.h"
#include "put_font.h"
#include "font_types.h"


void print_button(int x, int y, union pixel buffer[LCD_WIDTH][LCD_HEIGHT], bool is_select, int lvl_num){
	if(is_select){
		for(unsigned i = 0; i < HEIGHT; i++){
			buffer[x][y + i] = (union pixel){.d = 0xffff}; //left
			buffer[x + 1][y + i] = (union pixel){.d = 0xffff};
			buffer[x + WIDTH][y + i] = (union pixel){.d = 0xffff}; //right
			buffer[x - 1 + WIDTH][y + i] = (union pixel){.d = 0xffff};
		}
	
		for(unsigned i = 0; i < WIDTH; i++){
			buffer[x + i][y] = (union pixel){.d = 0xffff}; // top
			buffer[x + 1 + i][y + 1] = (union pixel){.d = 0xffff};
			buffer[x + i][y + HEIGHT] = (union pixel){.d = 0xffff}; //bottom
			buffer[x - 1 + i][y - 1 + HEIGHT] = (union pixel){.d = 0xffff};
		}
		print_button_text(x, y, buffer, lvl_num);
		
	}else{
		for(unsigned i = 0; i < HEIGHT; i++){
			buffer[x][y + i] = (union pixel){.d = 0x0}; //left
			buffer[x + 1][y + i] = (union pixel){.d = 0x0};
			buffer[x + WIDTH][y + i] = (union pixel){.d = 0x0}; //right
			buffer[x - 1 + WIDTH][y + i] = (union pixel){.d = 0x0};
		}
	
		for(unsigned i = 0; i < WIDTH; i++){
			buffer[x + i][y] = (union pixel){.d = 0x0}; // top
			buffer[x + 1 + i][y + 1] = (union pixel){.d = 0x0};
			buffer[x + i][y + HEIGHT] = (union pixel){.d = 0x0}; //bottom
			buffer[x - 1 + i][y - 1 + HEIGHT] = (union pixel){.d = 0x0};
		}
		print_button_text(x, y, buffer, lvl_num);
	}
}


int level_select(uint8_t new_value, uint8_t old_value, int select_button, union pixel menu_buffer[LCD_WIDTH][LCD_HEIGHT]){
	if(old_value > new_value && ((int)((int)old_value - (int)new_value) % 4 == 0)){
		select_button--;
	} else if(old_value < new_value && ((int)((int)old_value - (int)new_value) % 4 == 0)){
		select_button++;
	}else{
		select_button = select_button;
	}
	if(select_button < 0){
		select_button = 2;
	} else if (select_button > 2){
		select_button = 0;
	}
	
	switch(select_button){
		case 0:
			print_button((((LCD_WIDTH/2) - (WIDTH/2)) - 1), 
			(150 + (HEIGHT * 0) + 5), 
			menu_buffer, true, 1);
			print_button((((LCD_WIDTH/2) - (WIDTH/2)) - 1), 
			(150 + (HEIGHT * 1) + 5), 
			menu_buffer, false, 2);
			print_button((((LCD_WIDTH/2) - (WIDTH/2)) - 1), 
			(150 + (HEIGHT * 2) + 5), 
			menu_buffer, false, 3);
			break;
		case 1:
			print_button((((LCD_WIDTH/2) - (WIDTH/2)) - 1), 
			(150 + (HEIGHT * 0) + 5), 
			menu_buffer, false, 1);
			print_button((((LCD_WIDTH/2) - (WIDTH/2)) - 1), 
			(150 + (HEIGHT * 1) + 5), 
			menu_buffer, true, 2);
			print_button((((LCD_WIDTH/2) - (WIDTH/2)) - 1), 
			(150 + (HEIGHT * 2) + 5), 
			menu_buffer, false, 3);
			break;
		case 2:
			print_button((((LCD_WIDTH/2) - (WIDTH/2)) - 1), 
			(150 + (HEIGHT * 0) + 5), 
			menu_buffer, false, 1);
			print_button((((LCD_WIDTH/2) - (WIDTH/2)) - 1), 
			(150 + (HEIGHT * 1) + 5), 
			menu_buffer, false, 2);
			print_button((((LCD_WIDTH/2) - (WIDTH/2)) - 1), 
			(150 + (HEIGHT * 2) + 5), 
			menu_buffer, true, 3);
			break;
		}
	union pixel color;
	color.d = 0xffff;
	print_on_LCD(EXIT,menu_buffer, color, (LCD_HEIGHT - 16), ((LCD_WIDTH/2) - (strlen(INSTRUCTION)/2 * 8) - 8));
	print_on_LCD(INSTRUCTION, menu_buffer, color, (LCD_HEIGHT - 67), ((LCD_WIDTH/2) - (strlen(INSTRUCTION)/2 * 8) - 8));
	print_on_LCD(INSTRUCTION2, menu_buffer, color, (LCD_HEIGHT - 50), ((LCD_WIDTH/2) - (strlen(INSTRUCTION)/2 * 8) - 8));
	print_on_LCD(INSTRUCTION3, menu_buffer, color, (LCD_HEIGHT - 33), ((LCD_WIDTH/2) - (strlen(INSTRUCTION)/2 * 8) - 8));

	return select_button;
}



void print_button_text(int x, int y, union pixel buffer[LCD_WIDTH][LCD_HEIGHT], int lvl_num){
	union pixel color;
	color.d = PACMEH_COL;
	switch(lvl_num){
		case 1:
			print_on_LCD(LVL1, buffer, color, y + 1 + ((HEIGHT - 16)/2), (x + 10));
			break;
		case 2:
			print_on_LCD(LVL2, buffer, color, y + 1 + ((HEIGHT - 16)/2), (x + 10));
			break;
		case 3:
			print_on_LCD(LVL3, buffer, color, y + 1 + ((HEIGHT - 16)/2), (x + 10));
			break;
	}
}


void counter_print(int counter, union pixel menu_buffer[LCD_WIDTH][LCD_HEIGHT]){
	for(unsigned i = 0; i< strlen(COUNTER); i++){
		pchar(&font_rom8x16, COUNTER[i], (((strlen(COUNTER)/2 * 8) - 16) + i*(&font_rom8x16)->maxwidth) - 10, (0), menu_buffer, (union pixel){.d = 0xffff});
	}
	char text[20];
	sprintf(text, "%d", counter);
	for(unsigned x = ((strlen(COUNTER) * 10) - 18); x < ((strlen(COUNTER) * 12)); x++){
		for(unsigned y = 0; y < 16; y++){
			menu_buffer[x][y].d = 0x0;
		}
		
	}
	for(unsigned i = 0; i< strlen(text); i++){
		pchar(&font_rom8x16, text[i], (((strlen(COUNTER)/2 * 8) - 16) + (strlen(COUNTER) + i)*(&font_rom8x16)->maxwidth) - 8, (0), menu_buffer, (union pixel){.d = 0xffff}); //?
	}
}
