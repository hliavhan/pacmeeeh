/*******************************************************************
  image.c - simple function to redraw display
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "sprites.h"
#include "image.h"
unsigned char *parlcd_reg_base;

void redraw_display(union pixel buffer[LCD_WIDTH][LCD_HEIGHT]){
	//unsigned char *parlcd_reg_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0); // shall remove this allocation
	// reset location to the top left corner
	parlcd_write_cmd(parlcd_reg_base, 0x2c); 
	for(unsigned y = 0; y < LCD_HEIGHT; y++){
		for(unsigned x = 0; x < LCD_WIDTH; x++){
			parlcd_write_data(parlcd_reg_base, buffer[x][y].d);
		}
	}
}
