/*******************************************************************
  movement.c - changes (x, y) postiton of sprites.
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "pixels.h"
#include "sprites.h"
#include "movement.h"


int choose_dir(union pixel newbuf[LCD_WIDTH][LCD_HEIGHT], struct sprite_pixel sp_buf[], int dir_change)
{
	int iterations = PACMEH_SIZE;
	if(sp_buf[0].col != PACMEH_COL){
		// Calls for ghost movement which is a randomly change after collision with walls
		if(dir_change == STOP)
			dir_change = ghost_move();
		iterations = GHOST_SIZE;
	}
	switch (dir_change)
		{
			//-------------RIGHT-----------------
			case RIGHT:
				for (int k = 0; k < iterations; k++){
					if(newbuf[sp_buf[k].coord[0] + PIX_OD][sp_buf[k].coord[1]].d != 0x0)
					{
						dir_change = STOP;
						break;
					}

				}
				if(dir_change == STOP)
					break;
				for (int j = 0; j < iterations; j++)
				{
					if(sp_buf[j].coord[0] >= 480){ //change position when cross border of display
						sp_buf[j].coord[0] = sp_buf[j].coord[0]%480;
					
						}
					sp_buf[j].coord[0]+=PACMEH_SPEED;
				}
				break;
			//-------------LEFT-----------------	
			case LEFT:
				for (int k = 0; k < iterations; k++){ 
					if(newbuf[sp_buf[k].coord[0] - PIX_OD][sp_buf[k].coord[1]].d != BG_COL)
					{
						dir_change = STOP;
						break;
					}
				}
				if(dir_change == STOP)
					break;
				for (int j = 0; j < iterations; j++)
				{
					if(sp_buf[j].coord[0] <= 1){ //change position when cross border of display
						sp_buf[j].coord[0] = sp_buf[j].coord[0] + 480;
						} 
					sp_buf[j].coord[0]-=PACMEH_SPEED;
				}
				break;
			//-------------UP-----------------	
			case UP:
				for (int k = 0; k < iterations; k++){
					if(newbuf[sp_buf[k].coord[0]][sp_buf[k].coord[1] - PIX_OD].d != BG_COL)
					{
						dir_change = STOP;
						break;
					}
				}
				if(dir_change == STOP)
					break;
				for (int j = 0; j < iterations; j++)
				{
					if(sp_buf[j].coord[1] <= 1){ //change position when cross border of display
						sp_buf[j].coord[1] = sp_buf[j].coord[1] + 320;}
					sp_buf[j].coord[1]-=PACMEH_SPEED;
				}
				break;
			//-------------DOWN-----------------	
			case DOWN:
			for (int k = 0; k < iterations; k++){
				if(newbuf[sp_buf[k].coord[0]][sp_buf[k].coord[1] + PIX_OD].d != BG_COL){
					dir_change = STOP;
					break;
				}
			}
				if(dir_change == STOP)
					break;
				for (int j = 0; j < iterations; j++)
				{
					if(sp_buf[j].coord[1] >= 320){//change position when cross border of display
						sp_buf[j].coord[1] = sp_buf[j].coord[1] % 320;
						} 
					sp_buf[j].coord[1]+=PACMEH_SPEED;
				}
				break;
			//-------------STOP/DEFAULT-----------------	
			default:
				break;
		}
		return dir_change;
}


int ghost_move(){
	return rand() % 4;
}
