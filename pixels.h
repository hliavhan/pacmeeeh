/*******************************************************************
  pixel.h - reprezent pixel and led for display
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#ifndef PIXELS_H
#define PIXELS_H
union led{
	struct{
		uint8_t b,g,r;
	};
	uint32_t data;
};

// Pixels use 5-6-5 reprezentation
union pixel{
	struct{
		unsigned char b :5;
		unsigned char g :6;
		unsigned char r :5;
	};
	uint16_t d;
};
#endif 
