/*******************************************************************
  pacmeh.c - main function of the pac-meeeh game.
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/

#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h> 

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"
#include "pixels.h"
#include "put_font.h"
#include "sprites.h"
#include "menu.h"
#include "image.h"
#include "movement.h"
#include "hearths.h"

#define CONTINUE "TO CONTINUE GAME PRESS THE GREEN BUTTON"
#define QUIT "TO EXIT GAME PRESS THE RED BUTTON"

// Image of full level with all elements
union pixel loaded_full_level_map[LCD_WIDTH][LCD_HEIGHT];
// Base of level: black background and walls
union pixel level_map_base[LCD_WIDTH][LCD_HEIGHT];
// Map of food placed on map
union pixel food_map[LCD_WIDTH][LCD_HEIGHT];
//Map of big food places on map
union pixel big_food_map[LCD_WIDTH][LCD_HEIGHT];
// Buffer which is use for menu graphic
union pixel menu_buffer[LCD_WIDTH][LCD_HEIGHT];
union pixel free_pos_o_buffer[LCD_WIDTH][LCD_HEIGHT];
union pixel free_pos_b_buffer[LCD_WIDTH][LCD_HEIGHT];
union pixel free_pos_p_buffer[LCD_WIDTH][LCD_HEIGHT];

int counter;
int count = 0;

int reload_level(int select_button);

int main(int argc, char *argv[])
{
	union pixel color;
	uint32_t rgb_knobs_value;
	volatile void *spiled_reg_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    volatile uint32_t *ledline = (volatile uint32_t*)(spiled_reg_base + SPILED_REG_LED_LINE_o);
	rgb_knobs_value = *(volatile uint32_t*)(spiled_reg_base + SPILED_REG_KNOBS_8BIT_o);
	struct sprite_pixel ghost_o[14*14];
	struct sprite_pixel ghost_b[14*14];
	struct sprite_pixel ghost_p[14*14];
	struct sprite_pixel ghost_r[14*14];
	struct sprite_pixel pacmeeeeeh[12 * 13];
	
	parlcd_reg_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
	parlcd_write_cmd(parlcd_reg_base, 0x2c); // reset location to the top left corner
	uint8_t result[8];

	uint32_t rgb_copy_value = rgb_knobs_value;
	int tmp = RIGHT;
	int ghost_tmp1 = RIGHT, ghost_tmp2 = RIGHT, ghost_tmp3 = RIGHT, ghost_tmp4 = RIGHT;

	// Value of knobs
	uint8_t new_value, old_value;
	old_value = 0;
	new_value = 0;

	// Parametr to selection a type of level
	int select_button = 0;
	load_level_map("menu/menu.ppm", menu_buffer);
	print_button((((LCD_WIDTH/2) - (WIDTH/2)) - 1), 
		(150 + (HEIGHT * 0) + 5), 
		menu_buffer, true, 1);
	print_button((((LCD_WIDTH     /2) - (WIDTH/2)) - 1), 
		(150 + (HEIGHT * 1) + 5), 
		menu_buffer, false, 2);
	print_button((((LCD_WIDTH/2) - (WIDTH/2)) - 1), 
		(150 + (HEIGHT * 2) + 5), 
		menu_buffer, false, 3);

	// Menu loop
	while(1){
		redraw_display(menu_buffer);
		rgb_knobs_value = *(volatile uint32_t*)(spiled_reg_base + SPILED_REG_KNOBS_8BIT_o);
		if(((rgb_knobs_value & 0x0000ff00) >> 8) % 4 == 0)
		{
			old_value = (rgb_copy_value & 0x0000ff00) >> 8;
			new_value = (rgb_knobs_value & 0x0000ff00) >> 8;
			//diff_between_knobs += (int8_t)(new_value - old_value);
			//printf("%d\n", diff_between_knobs);
			//calculate_delta(&diff_between_knobs, &select_button, 2);
			select_button = level_select(new_value, old_value, select_button, menu_buffer);
			rgb_copy_value = rgb_knobs_value;
		}
		if((rgb_copy_value & 0xff000000) >> 24 == 0x04){
			load_level_map("menu/exit.ppm", menu_buffer);
			redraw_display(menu_buffer);
			exit(EXIT_SUCCESS);
		}
		if((rgb_copy_value & 0xff000000) >> 24 == 0x02){
			break;
		}

		
	}

	switch(select_button){
		case 0:
			load_level_map("level1/full_level.ppm", loaded_full_level_map);
			load_level_map("level1/background.ppm", level_map_base);
			counter = load_food_map("level1/food.ppm", food_map);
			load_level_map("level1/big_food.ppm", big_food_map);
			load_level_map("level1/orange_free.ppm", free_pos_o_buffer);
			load_level_map("level1/blue_free.ppm", free_pos_b_buffer);
			load_level_map("level1/pink_free.ppm", free_pos_p_buffer);
			break;
		case 1:
			load_level_map("level2/full_level.ppm", loaded_full_level_map);
			load_level_map("level2/background.ppm", level_map_base);
			counter = load_food_map("level2/food.ppm", food_map);
			load_level_map("level2/big_food.ppm", big_food_map);
			load_level_map("level2/orange_free.ppm", free_pos_o_buffer);
			load_level_map("level2/blue_free.ppm", free_pos_b_buffer);
			load_level_map("level2/pink_free.ppm", free_pos_p_buffer);
			break;
		case 2:
			load_level_map("level3/full_level.ppm", loaded_full_level_map);
			load_level_map("level3/background.ppm", level_map_base);
			counter = load_food_map("level3/food.ppm", food_map);
			load_level_map("level3/big_food.ppm", big_food_map);
			load_level_map("level3/orange_free.ppm", free_pos_o_buffer);
			load_level_map("level3/blue_free.ppm", free_pos_b_buffer);
			load_level_map("level3/pink_free.ppm", free_pos_p_buffer);
			break;
		default:
			printf("ERROR: This type of level has not been loaded.\n");
			exit(FILE_ERROR);
	}
	counter = counter/4;
	counter_print(count, level_map_base);
	sprite("paaac.ppm", loaded_full_level_map, pacmeeeeeh);
	sprite("ghost_orange.ppm", loaded_full_level_map, ghost_o);
	sprite("ghost_blue.ppm", loaded_full_level_map, ghost_b);
	sprite("ghost_red.ppm", loaded_full_level_map, ghost_r);
	sprite("ghost_pink.ppm", loaded_full_level_map, ghost_p);

	*ledline = 0xFFFFFFFF;
	for(unsigned i = 0; i < 7; i +=2) 
		result[i] = 0;
	
	// Game loop
	while(1)
	{
		parlcd_write_cmd(parlcd_reg_base, 0x2c);
		int i = 0, g1 = 0, g2 = 0, g3 = 0, g4 = 0;
		
		for(unsigned y = 0; y < LCD_HEIGHT; y++)
		{
			for(unsigned x = 0; x < LCD_WIDTH; x++)
			{
				
				if(i == PACMEH_SIZE)
					i = 0;
				// Collision with ghosts
				if((x == pacmeeeeeh[i].coord[0] && y == pacmeeeeeh[i].coord[1]) && 
				((x == ghost_o[g1].coord[0] && y == ghost_o[g1].coord[1]) ||
				(x == ghost_b[g2].coord[0] && y == ghost_b[g2].coord[1]) ||
				(x == ghost_r[g3].coord[0] && y == ghost_r[g3].coord[1]) ||
				(x == ghost_p[g4].coord[0] && y == ghost_p[g4].coord[1]))){
					i = 0;
					sprite("paaac.ppm", loaded_full_level_map, pacmeeeeeh);
					damage(ledline);
				}
				// Collision with food
				if(x == pacmeeeeeh[i].coord[0] && y == pacmeeeeeh[i].coord[1] && food_map[x][y].d != 0x0){
					parlcd_write_data(parlcd_reg_base, pacmeeeeeh[i].col);
					i++;
					counter--;
					count++;
					counter_print(count, level_map_base);
					if(count == counter){
						ghost_tmp2 = RIGHT, ghost_tmp3 = LEFT, ghost_tmp4 = RIGHT;
						sprite("ghost_orange.ppm", free_pos_o_buffer, ghost_o);
						sprite("ghost_blue.ppm", free_pos_b_buffer, ghost_b);
						sprite("ghost_pink.ppm", free_pos_p_buffer, ghost_p);
					}
					
					if(counter == 0){ 
						switch(select_button){
							case 2:
								load_level_map("menu/last_level_win.ppm", menu_buffer);
								redraw_display(menu_buffer);
								exit(EXIT_SUCCESS);
							default:
								
								color.d = 0xffff;
								load_level_map("menu/win.ppm", menu_buffer);
								print_on_LCD(CONTINUE, menu_buffer, color, (LCD_HEIGHT/2), ((LCD_WIDTH/2) - (strlen(EXIT)/2 * 8) - 8));
								print_on_LCD(QUIT, menu_buffer, color, (LCD_HEIGHT/2 + 20), ((LCD_WIDTH/2) - (strlen(QUIT)/2 * 8) - 8));
								redraw_display(menu_buffer);
								rgb_copy_value = rgb_knobs_value;
								while(1){
									rgb_knobs_value = *(volatile uint32_t*)(spiled_reg_base + SPILED_REG_KNOBS_8BIT_o);
									if((rgb_copy_value & 0xff000000) >> 24 == 0x04){
										load_level_map("menu/exit.ppm", menu_buffer);
										redraw_display(menu_buffer);
										exit(EXIT_SUCCESS);
									}
									if((rgb_copy_value & 0xff000000) >> 24 == 0x02){
										select_button = reload_level(select_button);
										break;
									}
									rgb_copy_value = rgb_knobs_value;						
								}
								counter = counter/4;
								count = 0;
								counter_print(count, level_map_base);
								sprite("paaac.ppm", loaded_full_level_map, pacmeeeeeh);
								sprite("ghost_orange.ppm", loaded_full_level_map, ghost_o);
								sprite("ghost_blue.ppm", loaded_full_level_map, ghost_b);
								sprite("ghost_red.ppm", loaded_full_level_map, ghost_r);
								sprite("ghost_pink.ppm", loaded_full_level_map, ghost_p);
								break;
							}
						}
					food_map[x][y].d = 0x0;
					food_map[x + 1][y].d = 0x0;
					food_map[x - 1][y].d = 0x0;
					food_map[x][y + 1].d = 0x0;
					food_map[x + 1][y + 1].d = 0x0;
					food_map[x - 1][y + 1].d = 0x0;
					food_map[x][y - 1].d = 0x0;
					food_map[x + 1][y - 1].d = 0x0;
					food_map[x - 1][y - 1].d = 0x0;
				} 
				else if(x == pacmeeeeeh[i].coord[0] && y == pacmeeeeeh[i].coord[1] && big_food_map[x][y].d != 0x0){
					parlcd_write_data(parlcd_reg_base, pacmeeeeeh[i].col);
					i++;
					big_food_map[x][y].d = 0x0;
					for(int u = 0; u < 10; u++){
						for(int t = 0; t < 10; t++){
							big_food_map[x + u][y + t].d = 0x0;
							big_food_map[x - u][y + t].d = 0x0;
							big_food_map[x + u][y - t].d = 0x0;
							big_food_map[x - u][y - t].d = 0x0;
							big_food_map[x + t][y + u].d = 0x0;
							big_food_map[x + t][y - u].d = 0x0;
							big_food_map[x - t][y + u].d = 0x0;
							big_food_map[x - t][y - u].d = 0x0;
						}
					}
				}
				else if(x == pacmeeeeeh[i].coord[0] && y == pacmeeeeeh[i].coord[1]){
					parlcd_write_data(parlcd_reg_base, pacmeeeeeh[i].col);
					i++;
				}
				else if(x == ghost_o[g1].coord[0] && y == ghost_o[g1].coord[1]){
					parlcd_write_data(parlcd_reg_base, ghost_o[g1].col);
					g1++;
				}
				else if(x == ghost_b[g2].coord[0] && y == ghost_b[g2].coord[1]){
					parlcd_write_data(parlcd_reg_base, ghost_b[g2].col);
					g2++;
				}
				else if(x == ghost_r[g3].coord[0] && y == ghost_r[g3].coord[1]){
					parlcd_write_data(parlcd_reg_base, ghost_r[g3].col);
					g3++;
				}
				else if(x == ghost_p[g4].coord[0] && y == ghost_p[g4].coord[1]){
					parlcd_write_data(parlcd_reg_base, ghost_p[g4].col);
					g4++;
				}
				else if(food_map[x][y].d != 0x0){
					parlcd_write_data(parlcd_reg_base, food_map[x][y].d);
				}
				else if(big_food_map[x][y].d != 0x0){
					parlcd_write_data(parlcd_reg_base, big_food_map[x][y].d);
				}
				else{	
					parlcd_write_data(parlcd_reg_base, level_map_base[x][y].d);
				}
			} 
		}
		rgb_knobs_value = *(volatile uint32_t*)(spiled_reg_base + SPILED_REG_KNOBS_8BIT_o);
		int x_ch = 0, y_ch = 0;
		result[4] = (rgb_knobs_value & 0x000000ff);
		result[5] = (rgb_knobs_value & 0x0000ff00) >> 8;
		result[6] = (rgb_knobs_value & 0x00ff0000) >> 16;
		result[7] = (rgb_knobs_value & 0xff000000) >> 24;
		
		if(result[0] != result[4] && (int)((int)result[0] - (int)result[4]) % 4 == 0)
		{
			if(result[0] < result[4]){
				x_ch = 1;
			}else{ x_ch = 2;}
			result[0] = (rgb_knobs_value & 0x000000ff);
		}
		else if(result[2] != result[6] && (int)((int)result[2] - (int)result[6]) % 4 == 0)
		{
			if(result[2] < result[6]){
				y_ch = 1;
			}else{ y_ch = 2;}
			result[2] = (rgb_knobs_value & 0x00ff0000) >> 16;
		}
		
		if(x_ch == 1)
			tmp = RIGHT;
		else if(x_ch == 2)
			tmp = LEFT;	
		else if(y_ch == 1)
			tmp = UP;
		else if(y_ch == 2)
			tmp = DOWN;
		rgb_copy_value = rgb_knobs_value;
		tmp = choose_dir(level_map_base, pacmeeeeeh, tmp);
		ghost_tmp1 = choose_dir(level_map_base, ghost_o, ghost_tmp1);
		ghost_tmp2 = choose_dir(level_map_base, ghost_b, ghost_tmp2);
		ghost_tmp3 = choose_dir(level_map_base, ghost_r, ghost_tmp3);
		ghost_tmp4 = choose_dir(level_map_base, ghost_p, ghost_tmp4);
	} 	
    return 0;
}

int reload_level(int select_button){
	if(select_button == 0){
		load_level_map("level2/full_level.ppm", loaded_full_level_map);
		load_level_map("level2/background.ppm", level_map_base);
		counter = load_food_map("level2/food.ppm", food_map);
		load_level_map("level2/big_food.ppm", big_food_map);
	} else{
		load_level_map("level3/full_level.ppm", loaded_full_level_map);
		load_level_map("level3/background.ppm", level_map_base);
		counter = load_food_map("level3/food.ppm", food_map);
		load_level_map("level3/big_food.ppm", big_food_map);
	}
	select_button++;
	return select_button;
}
