/*******************************************************************
  put_font.h - simple program which print fonts to display
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/
#include "font_types.h"
#include "pixels.h"
#include "sprites.h"
#include <string.h> 
#ifndef PUT_FONT_H
#define PUT_FONT_H
/// Function put char in buffer for print
///@param fdes font descriptor
///@param c char element
///@param x x position of left top corner on display
///@param y y position of left top corner on display
///@param buffer buffer of image in which will be added buttons
///@param color elemet color in RGB565 format
void pchar(font_descriptor_t *font, char c, unsigned x, unsigned y, union pixel buffer[480][320], union pixel color);

void print_on_LCD(char text[], union pixel menu_buffer[LCD_WIDTH][LCD_HEIGHT], union pixel color, int height, int width);
/// Function find widtn of char in particular font
///@param fdes font descriptor
///@param ch char number
unsigned char_width(font_descriptor_t* fdes, int ch);

#endif 
