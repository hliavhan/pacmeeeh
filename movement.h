/*******************************************************************
  movement.h - changes (x, y) postiton of sprites.
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "pixels.h"
#include "sprites.h"

#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3
#define STOP -1
#define NOT_DEF_DIR -100

#define PACMEH_SPEED 2
#define PIX_OD 3

#define PACMEH_SIZE 110
#define GHOST_SIZE 158

#ifndef MOVEMENT_H
#define MOVEMENT_H
/// Function controls movement of sprite element, update coordinats and check collisions
///@param newbuf buffered image of map
///@param sp_buf buffer wits sprite data
///@param dir_change diraction of movement
int choose_dir(union pixel newbuf[LCD_WIDTH][LCD_HEIGHT], struct sprite_pixel sp_buf[], int dir_change);
/// Randomly choose ghost direction 
int ghost_move();

#endif
