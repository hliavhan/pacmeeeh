# Pacmeeeh game
### Authors: _Nikolaenkov Grigorii, Hliavitskaya Hanna_

## Table Of Contents
- Introduction
- Features
- Tech

## Introduction
Sometimes when you are working with your single board computer you want to take rest of your work. 
And that's the time when you need our game!
That's a 3 level arcade, with the great prize at the end! We wrote this game with the team 

## Features

#### 1. Control
Control implented with rotate controllers at MZAPO board.
#### 2. Lights and effects
"How many hearths do i have?" if this the question that you asking at the start of the game, then we have an answer to you. LED line has 32 lights, each pack of 8 lights is one live!

## Tech
 
Technologies used: 
- [C](https://en.wikipedia.org/wiki/C_(programming_language)) - The whole project is written using C programming language
- [Git](https://gitlab.fel.cvut.cz/) - Version control system
- [VSCode](https://code.visualstudio.com/) - IDE 
 
Pacmeeeh itself is open source with a [public repository](https://gitlab.fel.cvut.cz/hliavhan/pacmeeeh) on GitLab.

