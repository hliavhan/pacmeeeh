/*******************************************************************
  hearths.h - control ledline, which reprezent HP of pac-meeeh
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
/// 1/4 of ledline is one heart of pac-meeeh
///@param ledline mapped ledline
void damage(volatile uint32_t *ledline);
