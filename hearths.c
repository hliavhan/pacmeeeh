/*******************************************************************
  hearths.c - control ledline, which reprezent HP of pac-meeeh
  (C) Copyright 2022 Nikolaenkov Grigorii, Hliavitskaya Hanna
 *******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "hearths.h"
#include "image.h"
#include "sprites.h"



void damage(volatile uint32_t *ledline){
	*ledline = *ledline << 8;
	if(*ledline == 0x0){
		union pixel buffer[LCD_WIDTH][LCD_HEIGHT];
		load_level_map("menu/gameover.ppm", buffer);
		redraw_display(buffer);
		// здесь не хватает меню gameover, надо дописатьЫ
		exit(EXIT_SUCCESS);		
	}
}

